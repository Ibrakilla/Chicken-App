/** Imports Modules */
import { Component, ViewChild } from "@angular/core";
import { Platform, Config, Nav } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { TranslateService } from "ng2-translate";
import { Storage } from "@ionic/storage";

@Component({
  templateUrl: "app.html"
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  /** Main root page */
  rootPage: string = "WelcomeComponent";

  /**
   * Define default page object variables
   * Dafault variable are -
   * @title       string      Side menu item title
   * @component   any         Any page component
   * @leftIcon    string      Menu item icon name
   */
  pages: Array<{ title: string; component: any; leftIcon: string }>;

  constructor(
    public platform: Platform,
    private config: Config,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    public translate: TranslateService,
    private storage: Storage
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      this.config.set("ios", "backButtonText", "");
    });

    // Set default translate language
    // Default language english
    // Language code 'en'
    translate.setDefaultLang("ar");
    this.platform.setDir("rtl", true);
    // Page navigation component
    this.pages = [
      { title: "الرئيسية", component: "HomeComponent", leftIcon: "md-home" },
      { title: "من نحن", component: "AboutUsComponent", leftIcon: "md-star" },
      {
        title: "منتجاتنا",
        component: "CategoryComponent",
        leftIcon: "md-checkbox"
      },
      {
        title: "المطالبات",
        component: "ClaimsComponent",
        leftIcon: "md-paper"
      },
      {
        title: "الشكاوي و المقترحات",
        component: "ComplaintsComponent",
        leftIcon: "information-circle"
      },

      { title: "فروعنا", component: "LocationComponent", leftIcon: "md-map" },
      {
        title: "تواصل معنا",
        component: "ContactUsComponent",
        leftIcon: "paper-plane"
      },
      {
        title: "مطالبات السيارات",
        component: "LoginComponent",
        leftIcon: "md-car"
      }
    ];
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if (page.component == "HomeComponent") {
      this.nav.setRoot(page.component);
    } else if (page.component == "LoginComponent") {
      this.storage.get("LoggedIn").then(LoggedIn => {
        console.log("LoggedIn: " + LoggedIn);
        if (LoggedIn) {
          this.nav.setRoot("PortalTabsPage");
        } else {
          console.log("LoggedIn: " + LoggedIn);
          this.nav.push("LoginComponent");
        }
      });
    } else {
      this.nav.push(page.component);
    }
  }
}
