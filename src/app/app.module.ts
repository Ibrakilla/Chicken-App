/** Imports Modules */
import { NgModule, ErrorHandler, Injectable, Injector } from "@angular/core";
import { IonicApp, IonicModule, IonicErrorHandler } from "ionic-angular";
import { BrowserModule } from "@angular/platform-browser";
import { HttpModule, Http } from "@angular/http";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { Pro } from "@ionic/pro";
import {
  TranslateModule,
  TranslateStaticLoader,
  TranslateLoader
} from "ng2-translate/ng2-translate";
import { Geolocation } from "@ionic-native/geolocation";
import { GoogleMaps } from "@ionic-native/google-maps";
import { HttpClientModule } from "@angular/common/http";
import { MyApp } from "./app.component";
import { RestProvider } from "../providers/rest/rest";
import { StorageServiceProvider } from "../providers/storage-service/storage-service";
import { IonicStorageModule } from "@ionic/storage";

import { PortalTabsPageModule } from "../pages/portal-tabs/portal-tabs.module";
import { PolicyPageModule } from "../pages/policy/policy.module";
import { NewClaimPageModule } from "../pages/new-claim/new-claim.module";

import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer";
import { File } from "@ionic-native/file";
import { Camera } from "@ionic-native/camera";
import { Transfer } from "@ionic-native/transfer";
import { FilePath } from "@ionic-native/file-path";
import { LoaderService } from "../common/services/loader.service";
import { DataTableModule } from "angular2-datatable";

const IonicPro = Pro.init("cbf65c12", {
  appVersion: "0.0.1"
});

@Injectable()
export class MyErrorHandler implements ErrorHandler {
  ionicErrorHandler: IonicErrorHandler;

  constructor(injector: Injector) {
    try {
      this.ionicErrorHandler = injector.get(IonicErrorHandler);
    } catch (e) {
      // Unable to get the IonicErrorHandler provider, ensure
      // IonicErrorHandler has been added to the providers list below
    }
  }

  handleError(err: any): void {
    IonicPro.monitoring.handleNewError(err);
    // Remove this if you want to disable Ionic's auto exception handling
    // in development mode.
    this.ionicErrorHandler && this.ionicErrorHandler.handleError(err);
  }

  static get parameters() {
    return [[Http], [Storage]];
  }
}

@NgModule({
  declarations: [MyApp],
  imports: [
    IonicModule.forRoot(MyApp, {
      menuType: "push",
      platforms: {
        ios: {
          menuType: "overlay"
        }
      }
    }),
    PortalTabsPageModule,
    PolicyPageModule,
    NewClaimPageModule,
    DataTableModule,
    BrowserModule,
    HttpModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    TranslateModule.forRoot({
      provide: TranslateLoader,
      useFactory: (http: Http) =>
        new TranslateStaticLoader(http, "assets/i18n", ".json"),
      deps: [Http]
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [MyApp],
  providers: [
    SplashScreen,
    StatusBar,
    GoogleMaps,
    Geolocation,
    IonicErrorHandler,
    RestProvider,
    StorageServiceProvider,
    Camera,
    File,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    FileTransfer,
    FileTransferObject,
    FilePath,
    Transfer,
    LoaderService
  ]
})
export class AppModule {}
