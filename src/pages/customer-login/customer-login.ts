/** Represents a Component of contactus page. */

/** Imports Modules */
import { Component, OnInit } from "@angular/core";
import { IonicPage } from "ionic-angular";
import { RestProvider } from "../../providers/rest/rest";
import {
  ToastController,
  NavController,
  LoadingController
} from "ionic-angular";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators
} from "@angular/forms";
import { StorageServiceProvider } from "../../providers/storage-service/storage-service";
import { Storage } from "@ionic/storage";
@IonicPage()
@Component({
  selector: "customer-login",
  templateUrl: "customer-login.html"
})
export class CustomerloginComponent implements OnInit {
  myForm: FormGroup;
  usersPolicyInfo: any;
  userInfo = { email: "", password: "" };

  constructor(
    public formBuilder: FormBuilder,
    public restProvider: RestProvider,
    public navCtrl: NavController,
    private toastCtrl: ToastController,
    private storage: Storage,
    private StorageServiceProvider: StorageServiceProvider,
    private readonly loadingCtrl: LoadingController
  ) {
    this.navCtrl = navCtrl;
  }

  presentSuccessToast() {
    let toast = this.toastCtrl.create({
      message: "تم ارسال الرسالة بنجاح",
      duration: 3000,
      position: "middle"
    });

    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });

    toast.present();
  }
  presentErrorToast() {
    let toast = this.toastCtrl.create({
      message: "بيانات الدخول غير صحيحة، الرجاء المحاولة مرة أخرى",
      duration: 3000,
      position: "middle"
    });

    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });

    toast.present();
  }
  ngOnInit(): any {
    this.myForm = this.formBuilder.group({
      email: [
        "",
        [
          Validators.required,
          Validators.minLength(1),
          this.emailValidator.bind(this)
        ]
      ],
      password: [
        "",
        [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(300),
          this.passwordValidator.bind(this)
        ]
      ]
    });
  }

  onSubmit() {
    let loader = this.loadingCtrl.create({
    });
    loader.present();
    this.restProvider.login(this.userInfo.email, this.userInfo.password).then(
      result => {
        console.log(result);
        if (Object.keys(result).length == 0) {
          this.presentErrorToast();
          loader.dismiss();
        } else {
          this.usersPolicyInfo = result;
          this.storage.set("LoggedIn", true);
          this.storage.set("Class", this.usersPolicyInfo[0].Class);
          this.storage.set("ClientID", this.usersPolicyInfo[0].ClientID);
          this.storage.set("ClientName", this.usersPolicyInfo[0].ClientName);
          this.storage.set("CreatedOn", this.usersPolicyInfo[0].CreatedOn);
          this.storage.set("Email", this.usersPolicyInfo[0].Email);
          this.storage.set("Mobile", this.usersPolicyInfo[0].Mobile);
          this.storage.set("Password", this.usersPolicyInfo[0].Password);
          this.storage.set("Phone", this.usersPolicyInfo[0].Phone);
          this.storage.set("Type", this.usersPolicyInfo[0].Type);
          console.log(this.usersPolicyInfo[0].ClientName);
          loader.dismiss();
          this.navCtrl.setRoot("PortalTabsPage");
        }
      },
      err => {
        this.presentErrorToast();
        loader.dismiss();
        console.log(err);
      }
    );
  }

  isValid(field: string) {
    let formField = this.myForm.get(field);
    return formField.valid || formField.pristine;
  }

  emailValidator(control: FormControl): { [s: string]: boolean } {
    if (
      !control.value
        .toLowerCase()
        .match(
          "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"
        )
    ) {
      return { invalidEmail: true };
    }
  }
  passwordValidator(control: FormControl): { [s: string]: boolean } {
    if (!control.value.match("^[a-zA-Z ,\u0600-\u06FF,.'-,\\d]+$")) {
      return {};
    }
  }
  gotoTargetComponent(value) {
    if (value === "New-Account") {
      // When value is Profile
      this.storage.clear();
      this.navCtrl.push("NewAccountComponent");
    } else if (value === "AboutUs") {
      // When value is About Us
      this.navCtrl.push("AboutUsComponent");
    } else if (value === "claims") {
      // When value is About Us
      this.navCtrl.push("ClaimsComponent");
    } else if (value === "Location") {
      // When value is About Us
      this.navCtrl.push("LocationComponent");
    } else if (value === "ContactUs") {
      // When value is About Us
      this.navCtrl.push("ContactUsComponent");
    } else if (value === "Complaints") {
      // When value is About Us
      this.navCtrl.push("ComplaintsComponent");
    } else if (value === "login") {
      // When value is About Us
      this.storage.get("LoggedIn").then(LoggedIn => {
        console.log("LoggedIn: " + LoggedIn);
        if ((LoggedIn)) {
          this.navCtrl.setRoot("PortalTabsPage");
        } else {
          console.log("LoggedIn: " + LoggedIn);
          this.navCtrl.push("LoginComponent");
        }
      });
    }
  }
}
