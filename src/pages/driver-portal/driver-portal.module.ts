import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { DriverPortalComponent } from './driver-portal';

@NgModule({
  declarations: [
    DriverPortalComponent,
  ],
  imports: [
    IonicPageModule.forChild(DriverPortalComponent),
    TranslateModule
  ],
  exports: [
    DriverPortalComponent,
    TranslateModule
  ]
})
export class DriverPortalModule {}
