/** Represents a Component of AboutUs. */

/** Imports Modules */
import { Component } from "@angular/core";
import { NavController, IonicPage, ModalController } from "ionic-angular";
import { App,MenuController  } from "ionic-angular";

@IonicPage()
@Component({
  selector: "driver-portal",
  templateUrl: "driver-portal.html"
})
export class DriverPortalComponent {
  segment: any;
  constructor(
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public appCtrl: App
  ) {
    this.segment = "design";
    this.navCtrl = navCtrl;

  }
  goBack() {
    let modal = this.modalCtrl.create("HomeComponent");
    modal.present();
  }
  Logout() {
    this.appCtrl.getRootNavs()[0].push("WelcomeComponent");
  }

  gotoTargetComponent(value) {
    if (value === "Category") {
      // When value is Profile
      this.navCtrl.push("CategoryComponent");
    } else if (value === "AboutUs") {
      // When value is About Us
      this.navCtrl.push("AboutUsComponent");
    } else if (value === "claims") {
      // When value is About Us
      this.navCtrl.push("ClaimsComponent");
    } else if (value === "Location") {
      // When value is About Us
      this.navCtrl.push("LocationComponent");
    } else if (value === "ContactUs") {
      // When value is About Us
      this.navCtrl.push("ContactUsComponent");
    } else if (value === "Complaints") {
      // When value is About Us
      this.navCtrl.push("ComplaintsComponent");}
  }
}
