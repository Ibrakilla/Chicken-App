import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { TranslateModule } from "ng2-translate/ng2-translate";
import { HomeComponent } from "./home";

@NgModule({
  declarations: [HomeComponent],
  imports: [IonicPageModule.forChild(HomeComponent), TranslateModule],
  exports: [HomeComponent, TranslateModule]
})
export class HomeModule {}
