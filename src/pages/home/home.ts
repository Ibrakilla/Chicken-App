/** Represents a Component of Components page. */

/** Import Modules */
import { Component } from "@angular/core";
import { NavController, IonicPage, ModalController } from "ionic-angular";
import { Storage } from "@ionic/storage";
@IonicPage()
@Component({
  selector: "home",
  templateUrl: "home.html"
})
export class HomeComponent {
  logged: string;
  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    private storage: Storage
  ) {
    this.navCtrl = navCtrl;
    storage.set("name", "Max");
  }

  /**
   * gotoTargetComponent(value)
   * This function works on click event
   * when user click any component its get the value of this specific event and
   * NavController set this component and will take user to that component
   */
  gotoTargetComponent(value) {
    if (value === "Category") {
      // When value is Profile
      this.navCtrl.push("CategoryComponent");
    } else if (value === "AboutUs") {
      // When value is About Us
      this.navCtrl.push("AboutUsComponent");
    } else if (value === "claims") {
      // When value is About Us
      this.navCtrl.push("ClaimsComponent");
    } else if (value === "Location") {
      // When value is About Us
      this.navCtrl.push("LocationComponent");
    } else if (value === "ContactUs") {
      // When value is About Us
      this.navCtrl.push("ContactUsComponent");
    } else if (value === "Complaints") {
      // When value is About Us
      this.navCtrl.push("ComplaintsComponent");
    } else if (value === "login") {
      // When value is About Us
      this.storage.get("LoggedIn").then(LoggedIn => {
        console.log("LoggedIn: " + LoggedIn);
        if ((LoggedIn)) {
          this.navCtrl.setRoot("PortalTabsPage");
        } else {
          console.log("LoggedIn: " + LoggedIn);
          this.navCtrl.push("LoginComponent");
        }
      });
    }
  }
}
