/** Represents a Component of AboutUs. */

/** Imports Modules */
import { Component, ViewChild, ElementRef } from "@angular/core";
import { NavController, IonicPage, ModalController } from "ionic-angular";
import { Platform } from "ionic-angular/platform/platform";

declare var google: any;

@IonicPage()
@Component({
  selector: "location",
  templateUrl: "location.html",
  providers: []
})
export class LocationComponent {
  @ViewChild("map") mapRef: ElementRef;
  segment: any;
  constructor(
    public platform: Platform,
    public modalCtrl: ModalController,
    public navCtrl: NavController
  ) {
    this.segment = "design";
    this.navCtrl = navCtrl;
  }

  goBack() {
    let modal = this.modalCtrl.create("HomeComponent");
    modal.present();
  }
  ionViewDidLoad() {
    console.log(this.mapRef);
    this.loadMap();
  }

  loadMap() {
    const location = new google.maps.LatLng(27.510713, 41.716701);

    const options = {
      center: location,
      streetViewControl: false,
      zoom: 15
    };

    var locations = [
    ];
    var infowindow = new google.maps.InfoWindow();

    var marker, i;
    const map = new google.maps.Map(this.mapRef.nativeElement, options);
    for (i = 0; i < locations.length; i++) {
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }

    this.addMarker(location, map);
  }
  addMarker(position, map) {
    return new google.maps.Marker({
      position,
      map
    });
  }
}
