import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { NewAccountComponent } from './new-account';

@NgModule({
  declarations: [
    NewAccountComponent,
  ],
  imports: [
    IonicPageModule.forChild(NewAccountComponent),
    TranslateModule
  ],
  exports: [
    NewAccountComponent,
    TranslateModule
  ]
})
export class NewAccountModule {}
