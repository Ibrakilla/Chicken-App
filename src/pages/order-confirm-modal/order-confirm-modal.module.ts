import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderConfirmModalPage } from './order-confirm-modal';

@NgModule({
  declarations: [
    OrderConfirmModalPage,
  ],
  imports: [
    IonicPageModule.forChild(OrderConfirmModalPage),
  ],
})
export class OrderConfirmModalPageModule {}
