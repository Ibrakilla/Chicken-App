import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from "ionic-angular";
import { App, MenuController } from "ionic-angular";
import { Storage } from "@ionic/storage";
import { StorageServiceProvider } from "../../providers/storage-service/storage-service";
import { ToastController, LoadingController } from "ionic-angular";
import { RestProvider } from "../../providers/rest/rest";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ModalController } from "ionic-angular";
import { AlertController } from "ionic-angular";

/**
 * Generated class for the OrderConfirmModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-order-confirm-modal",
  templateUrl: "order-confirm-modal.html"
})
export class OrderConfirmModalPage {
  OrderSNo: any;
  userPendingOrders: any;
  usersClass: string;
  segment: any;
  apiPendingOrders = "http://localhost:51122/api/customer/GetClientPendingOrders";
  apiSentOrders = "http://localhost:51122/api/customer/GetClientSentOrders";
  apiOrderConfirmation = "http://localhost:51122/api/customer/PostOrderConfirmation";
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public view: ViewController,
    public restProvider: RestProvider,
    public appCtrl: App,
    private menu: MenuController,
    private storage: Storage,
    public modalCtrl: ModalController,
    private storageServiceProvider: StorageServiceProvider,
    private toastCtrl: ToastController,
    private StorageServiceProvider: StorageServiceProvider,
    private readonly loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public http: HttpClient
  ) {}
  ConfirmOrder() {
    const myData = this.OrderSNo;
    this.view.dismiss(myData);
  }
  ionViewDidLoad() {
    console.log("ionViewDidLoad OrderConfirmModalPage");
    this.OrderSNo = this.navParams.get("data");
  }
  closeModal() {
    const myData = "No";
    this.view.dismiss(myData);
  }

  ionViewWillLoad() {}
}
