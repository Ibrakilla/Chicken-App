import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { OrderDetailsComponent } from './order-details';

@NgModule({
  declarations: [
    OrderDetailsComponent,
  ],
  imports: [
    IonicPageModule.forChild(OrderDetailsComponent),
    TranslateModule
  ],
  exports: [
    OrderDetailsComponent,
    TranslateModule
  ]
})
export class OrderDetailsModule {}
