/** Represents a Component of AboutUs. */

/** Imports Modules */
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { App, MenuController } from "ionic-angular";
import { Storage } from "@ionic/storage";
import { StorageServiceProvider } from "../../providers/storage-service/storage-service";
import { ToastController, LoadingController } from "ionic-angular";
import { RestProvider } from "../../providers/rest/rest";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ModalController } from "ionic-angular";
@IonicPage()
@Component({
  selector: "order-details",
  templateUrl: "order-details.html"
})
export class OrderDetailsComponent {
  Order: any;
  OrderItems: any;
  usersClass: string;
  apiOrder = "http://upappapi.com/api/customer/GetOrderBySNo";
  apiOrderDetails = "http://upappapi.com/api/customer/GetOrderItemsBySNo";
  constructor(
    public navCtrl: NavController,
    public restProvider: RestProvider,
    public navParams: NavParams,
    public appCtrl: App,
    private menu: MenuController,
    private storage: Storage,
    private storageServiceProvider: StorageServiceProvider,
    private toastCtrl: ToastController,
    private StorageServiceProvider: StorageServiceProvider,
    private readonly loadingCtrl: LoadingController,
    public http: HttpClient
  ) {
    let loader = this.loadingCtrl.create({});
    loader.present();
      this.storage.get("OrderSNo").then(OrderSNo => {
        this.http
          .get(this.apiOrder + "?sno=" + OrderSNo, {
            headers: new HttpHeaders().set("Content-Type", "application/json")
          })
          .subscribe(
            res => {
              console.log(res);
              this.Order = res;
              // this.usersClass = Class;
              this.GetOrderItems();
              this.storage.get("OrderSNo").then(OrderSNo => {
                this.http
                  .get(this.apiOrderDetails + "?sno=" + OrderSNo, {
                    headers: new HttpHeaders().set("Content-Type", "application/json")
                  })
                  .subscribe(
                    res => {
                      console.log(res);
                      this.OrderItems = res;
                      // this.usersClass = Class;
                      loader.dismiss();
                    },
                    err => {
                      console.log(err);
                      loader.dismiss();
                    }
                  );
              });
            },
            err => {
              console.log(err);
              loader.dismiss();
            }
          );
      });
  }
  GetOrderItems(){

}
  ShowOrderDetails(orderSNo) {
    this.storage.set("OrderSNo", orderSNo.SNo).then(OrderSNo => {
      console.log("email: " + OrderSNo);
      this.appCtrl.getRootNavs()[0].push("OrderDetailsComponent");
    });
    console.log("User Order SNo" + orderSNo.SNo);
  }

  SendConfirmation(orderSNo){

  }




  ionViewDidEnter() {
    this.menu.swipeEnable(false);

    // If you have more than one side menu, use the id like below
    // this.menu.swipeEnable(false, 'menu1');
  }

  ionViewDidLeave() {
    // Don't forget to return the swipe to normal, otherwise
    // the rest of the pages won't be able to swipe to open menu
    this.menu.swipeEnable(true);

    // If you have more than one side menu, use the id like below
    // this.menu.swipeEnable(true, 'menu1');
  }
  ionViewDidLoad() {
    console.log("ionViewDidLoad PolicyPage");
  }
  Logout() {
    this.storage.clear();
    this.appCtrl.getRootNavs()[0].push("WelcomeComponent");
  }
  GoBack() {
    this.storage.set("LoggedIn", true);
    this.appCtrl.getRootNavs()[0].push("HomeComponent");
  }

  presentSuccessToast() {
    let toast = this.toastCtrl.create({
      message: "تم ارسال الرسالة بنجاح",
      duration: 3000,
      position: "middle"
    });

    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });

    toast.present();
  }
  presentErrorToast() {
    let toast = this.toastCtrl.create({
      message: "بيانات الدخول غير صحيحة، الرجاء المحاولة مرة أخرى",
      duration: 3000,
      position: "middle"
    });

    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });

    toast.present();
  }
}
