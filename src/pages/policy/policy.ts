import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { App, MenuController } from "ionic-angular";
import { Storage } from "@ionic/storage";
import { StorageServiceProvider } from "../../providers/storage-service/storage-service";
import { ToastController, LoadingController } from "ionic-angular";
import { RestProvider } from "../../providers/rest/rest";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ModalController } from "ionic-angular";
import { AlertController } from "ionic-angular";
/**
 * Generated class for the PolicyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-policy",
  templateUrl: "policy.html"
})
export class PolicyPage {
  userSentOrders: any;
  userPendingOrders: any;
  usersClass: string;
  segment: any;
  apiPendingOrders = "http://upappapi.com/api/customer/GetClientPendingOrders";
  apiSentOrders = "http://upappapi.com/api/customer/GetClientSentOrders";
  apiOrderConfirmation = "http://upappapi.com/api/customer/PostOrderConfirmation";
  constructor(
    public navCtrl: NavController,
    public restProvider: RestProvider,
    public navParams: NavParams,
    public appCtrl: App,
    private menu: MenuController,
    private storage: Storage,
    public modalCtrl: ModalController,
    private storageServiceProvider: StorageServiceProvider,
    private toastCtrl: ToastController,
    private StorageServiceProvider: StorageServiceProvider,
    private readonly loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public http: HttpClient
  ) {
    this.GetAllOrders();
  }

  GetAllOrders() {
    console.log("GetAllOrders");
    let loader = this.loadingCtrl.create({});
    loader.present();
    this.storage.get("ClientID").then(ClientID => {
      this.storage.get("Class").then(Class => {
        this.http
          .get(this.apiPendingOrders + "?ClientID=" + ClientID, {
            headers: new HttpHeaders().set("Content-Type", "application/json")
          })
          .subscribe(
            res => {
              console.log(res);
              this.userPendingOrders = res;
              // this.usersClass = Class;
              console.log(this.userPendingOrders[0].Driver);
              this.storage.set(
                "OrderDriverName",
                this.userPendingOrders[0].Driver
              );
              this.GetSentOrders();
              this.segment = "design";
              loader.dismiss();
            },
            err => {
              console.log(err);
              loader.dismiss();
            }
          );
      });
    });
  }
  GetSentOrders() {
    this.storage.get("ClientID").then(ClientID => {
      this.storage.get("Class").then(Class => {
        this.http
          .get(this.apiSentOrders + "?ClientID=" + ClientID, {
            headers: new HttpHeaders().set("Content-Type", "application/json")
          })
          .subscribe(
            res => {
              console.log(res);
              this.userSentOrders = res;
              // this.usersClass = Class;
            },
            err => {
              console.log(err);
            }
          );
      });
    });
  }
  ShowOrderDetails(orderSNo) {
    this.storage.set("OrderSNo", orderSNo.SNo).then(OrderSNo => {
      console.log("email: " + OrderSNo);
      this.appCtrl.getRootNavs()[0].push("OrderDetailsComponent");
    });
    console.log("User Order SNo" + orderSNo.SNo);
  }

  SendConfirmation(orderSNo) {
    const myData = orderSNo.SNo;
    const myModal = this.modalCtrl.create("OrderConfirmModalPage", {
      data: myData
    });
    myModal.present();
    myModal.onDidDismiss(data => {
      console.log("MODAL RESPONSE : " + data);
      if (data == "No") {
        console.log("Modal Sed No");
      } else {
        console.log("Modal Sed Yes");
        let loader = this.loadingCtrl.create({});
        loader.present();
        this.storage.get("OrderDriverName").then(OrderDriverName => {
          this.storage.get("ClientName").then(ClientName => {
            let postParams = { SNo: data, ClientName: ClientName, Driver:OrderDriverName };
            this.http
              .post(
                this.apiOrderConfirmation,
                postParams,
                {
                  headers: new HttpHeaders().set(
                    "Content-Type",
                    "application/json"
                  )
                }
              )
              .subscribe(
                res => {
                  console.log(res);
                  console.log("Alert DISMISSED");

                  // this.usersClass = Class;
                },
                err => {
                  console.log(err);
                }
              );
          });
        });
        setTimeout(() => {
          loader.dismiss();
          this.presentSuccessToast();
          this.navCtrl.setRoot(this.navCtrl.getActive().component);
        }, 2700);
      }
    });
  }

  ionViewDidEnter() {
    this.menu.swipeEnable(false);

    // If you have more than one side menu, use the id like below
    // this.menu.swipeEnable(false, 'menu1');
  }

  ionViewDidLeave() {
    // Don't forget to return the swipe to normal, otherwise
    // the rest of the pages won't be able to swipe to open menu
    this.menu.swipeEnable(true);

    // If you have more than one side menu, use the id like below
    // this.menu.swipeEnable(true, 'menu1');
  }
  ionViewDidLoad() {
    console.log("ionViewDidLoad PolicyPage");
  }
  Logout() {
    this.storage.clear();
    this.appCtrl.getRootNavs()[0].push("WelcomeComponent");
  }
  GoBack() {
    this.storage.set("LoggedIn", true);
    this.appCtrl.getRootNavs()[0].push("HomeComponent");
  }

  presentSuccessToast() {
    let toast = this.toastCtrl.create({
      message: "تم تأكيد إستلام الطلب",
      duration: 3000,
      position: "middle"
    });

    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });

    toast.present();
  }
  presentErrorToast() {
    let toast = this.toastCtrl.create({
      message: "خطأ في الإرسال, الرجاء إعادة المحاولة.",
      duration: 3000,
      position: "middle"
    });

    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });

    toast.present();
  }
}
