/** Represents a Component of Components page. */

/** Import Modules */
import { Component } from "@angular/core";
import { NavController, IonicPage, ModalController } from "ionic-angular";
import { Storage } from "@ionic/storage";
import { RestProvider } from "../../providers/rest/rest";
@IonicPage()
@Component({
  selector: "welcome",
  templateUrl: "welcome.html"
})
export class WelcomeComponent {
  logged: string;
  usersPolicyInfo: any;
  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    private storage: Storage,
    private restProvider: RestProvider
  ) {
    this.storage.get("LoggedIn").then(loggedIn => {
      console.log("LoggedIn: " + loggedIn);
    });
    this.navCtrl = navCtrl;
    storage.set("name", "Max");
  }

  /**
   * gotoTargetComponent(value)
   * This function works on click event
   * when user click any component its get the value of this specific event and
   * NavController set this component and will take user to that component
   */

  gotoTargetComponent(value) {
    if (value === "CustomerLogin") {
      this.storage.get("LoggedIn").then(LoggedIn => {
        console.log("LoggedIn: " + LoggedIn);
        if (LoggedIn) {
          this.storage.get("Email").then(Email => {
            this.storage.get("Password").then(Password => {
              this.restProvider.login(Email, Password).then(
                result => {
                  console.log(result);
                  if (Object.keys(result).length == 0) {
                  } else {
                    this.usersPolicyInfo = result;
                    this.storage.set("LoggedIn", true);
                    this.storage.set("Class", this.usersPolicyInfo[0].Class);
                    this.storage.set(
                      "ClientID",
                      this.usersPolicyInfo[0].ClientID
                    );
                    this.storage.set(
                      "ClientName",
                      this.usersPolicyInfo[0].ClientName
                    );
                    this.storage.set(
                      "CreatedOn",
                      this.usersPolicyInfo[0].CreatedOn
                    );
                    this.storage.set("Email", this.usersPolicyInfo[0].Email);
                    this.storage.set("Mobile", this.usersPolicyInfo[0].Mobile);
                    this.storage.set(
                      "Password",
                      this.usersPolicyInfo[0].Password
                    );
                    this.storage.set("Phone", this.usersPolicyInfo[0].Phone);
                    this.storage.set("Type", this.usersPolicyInfo[0].Type);
                    console.log(this.usersPolicyInfo[0].ClientName);
                    this.navCtrl.setRoot("PortalTabsPage");
                  }
                },
                err => {
                  console.log(err);
                  this.storage.clear();
                  this.navCtrl.push("CustomerloginComponent");
                }
              );

              this.navCtrl.setRoot("PortalTabsPage");
            });
          });
        } else {
          console.log("LoggedIn: " + LoggedIn);
          this.storage.clear();
          this.navCtrl.push("CustomerloginComponent");
        }
      });

      // When value is About Us
    } else if (value === "CustomerPortal") {
      // When value is About Us
      this.navCtrl.setRoot("PortalTabsPage");
    } else if (value === "DriverLogin") {
      // When value is About Us
      this.navCtrl.push("DriverLoginComponent");
    }
  }
}
