import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";
/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestProvider {
  apiEmail = "http://upappapi.com/api/email/PostComplaint";
  apiLogin = "http://upappapi.com/api/customer/GetClientInfo";
  apiPendingOrders = "http://upappapi.com/api/customer/GetClientPendingOrders";
  apiSentOrders = "http://upappapi.com/api/customer/GetClientSentOrders";
  constructor(public http: HttpClient, public storage: Storage) {
    console.log("Hello RestProvider Provider");
  }
  ApiMsg: any;
  msgApi: any;
  ClientName: string;
  PolicyNo: string;
  PeriodFrom: string;
  PeriodTo: string;
  PlateNo: string;
  PolicyHolder: string;
  SNo: string;
  addUser(data2, data) {
    this.msgApi =
      "Name: " +
      data.name +
      "<br />" +
      "Email: " +
      data.email +
      "<br />" +
      "Phone: " +
      data.phone +
      "<br />" +
      "Comment: " +
      data.comment;

    this.ApiMsg = {
      To: "ibrahim_elhussein@hotmail.com",
      Cc: "info@oasisoft.net",
      Subject: "Elnilein Mobile App - Contact Form",
      Body: this.msgApi
    };

    console.log(data);
    console.log(data2);
    console.log(this.msgApi);
    return new Promise((resolve, reject) => {
      this.http
        .post(this.apiEmail, this.ApiMsg, {
          headers: new HttpHeaders().set("Content-Type", "application/json")
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }

  addUser2(data2, data) {
    this.msgApi =
      "Name: " +
      data.name +
      "<br />" +
      "Email: " +
      data.email +
      "<br />" +
      "Phone: " +
      data.phone +
      "<br />" +
      "Notes: " +
      data.comment;

    this.ApiMsg = {
      To: "ibrahim_elhussein@hotmail.com",
      Cc: "info@oasisoft.net",
      Subject: "Elnilein Mobile App - Complaints Form",
      Body: this.msgApi
    };

    console.log(data);
    console.log(data2);
    console.log(this.msgApi);
    return new Promise((resolve, reject) => {
      this.http
        .post(this.apiEmail, this.ApiMsg, {
          headers: new HttpHeaders().set("Content-Type", "application/json")
        })
        .subscribe(
          res => {
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }

  login(data, data2) {
    console.log(data.policyno);
    JSON.stringify(data);
    return new Promise((resolve, reject) => {
      this.http
        .get(this.apiLogin + "?email=" + data + "&password=" + data2 + "", {
          headers: new HttpHeaders().set("Content-Type", "application/json")
        })
        .subscribe(
          res => {
            console.log(res);
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }



}
