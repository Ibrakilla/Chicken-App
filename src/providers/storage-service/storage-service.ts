import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import "rxjs/add/operator/map";
import { Storage } from "@ionic/storage";
/*
  Generated class for the StorageServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class StorageServiceProvider {

  constructor(public http: Http, private storage: Storage) {
    console.log("Hello Localstorage Providerrrrrrrrrrr");
  }

  UserInfo(
    Class,
    ClientID,
    ClientName,
    CreatedOn,
    Email,
    Mobile,
    Password,
    Phone,
    Type
  ) {
    this.storage.set("LoggedIn", true);
    this.storage.set("Class", Class);
    this.storage.set("ClientID", ClientID);
    this.storage.set("ClientName", ClientName);
    this.storage.set("CreatedOn", CreatedOn);
    this.storage.set("Email", Email);
    this.storage.set("Mobile", Mobile);
    this.storage.set("Password", Password);
    this.storage.set("Phone", Phone);
    this.storage.set("Type", Type);

    this.storage.get("ClientID").then(ClientID => {
      console.log("Storage API"+ClientID);
    });
  }
  //store the email address
  setEmail(email) {
    this.storage.set("email", email);
  }

  //get the stored email
  getEmail() {
    this.storage.get("email").then(email => {
      console.log("email: " + email);
    });
  }

  //delete the email address
  removeEmail() {
    this.storage.remove("email").then(() => {
      console.log("email is removed");
    });
  }

  //clear the whole local storage
  clearStorage() {
    this.storage.clear().then(() => {
      console.log("all keys are cleared");
    });
  }
}
